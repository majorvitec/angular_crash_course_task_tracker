# Task Tracker [2021]

Created a Task Tracker Single Page Application [Followed Tutorial].

## Source
[Angular Crash Course 2021](https://www.youtube.com/watch?v=3dHNOWTI7H8)

## Dependencies
- [json-server](https://www.npmjs.com/package/json-server)

## Project Description
- Angular version 12.2.1
- Navigate to About Page
- ADD | REMOVE Tasks

## Development server

- Run `ng serve` for a dev server
- Navigate to `http://localhost:4200/`
- The app will automatically reload if you change any of the source files

## Json server [REQUIERED]

- Run `npm run server` to start json server
- Watch-Command is set in `package.json` file
- API-URL is set to `http://localhost:5000` in `task.service.ts` file.

## Pages Overview

### Tasks Page
![Tasks Page ](/images/readme/main_page.png "Tasks Page")

### Add Task Page
![Add Task Page](/images/readme/add_task_page.png "Add Task Page")

### Task Added Page
![Task Added Page](/images/readme/task_added_page.png "Task Added Page")

### About Page
![About Page](/images/readme/about_page.png "About Page")
